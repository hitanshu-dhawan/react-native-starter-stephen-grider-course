import React, { useState } from "react";
import { View, Text, Button } from "react-native";

const CounterScreen = () => {

    const [counter, setCounter] = useState(0);

    return (
        <View>
            <Button
                title="++++"
                onPress={() => {
                    setCounter(counter + 1);
                }} />
            <Button
                title="----"
                onPress={() => {
                    setCounter(counter - 1);
                }} />
            <Text>{counter}</Text>
        </View>
    );
};

export default CounterScreen;