import React from "react";
import { Text, StyleSheet, View, Button, TouchableOpacity } from "react-native";

const HomeScreen = (props) => {
  return (
    <View>
      <Text style={styles.text}>HomeScreen</Text>
      <Button
        title="Go to Components Screen"
        onPress={() => props.navigation.navigate('Components')}
      />
      <TouchableOpacity onPress={() => props.navigation.navigate('List')}>
        <Text style={styles.text}>Go to List Screen</Text>
      </TouchableOpacity>
      <Button
        title="Go to Images Screen"
        onPress={() => props.navigation.navigate('Image')}
      />
       <Button
        title="Go to Counter Screen"
        onPress={() => props.navigation.navigate('Counter')}
      />
       <Button
        title="Go to Colored Screen"
        onPress={() => props.navigation.navigate('ColoredScreen')}
      />
        <Button
        title="Go to Text Screen"
        onPress={() => props.navigation.navigate('TextScreen')}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30
  }
});

export default HomeScreen;

