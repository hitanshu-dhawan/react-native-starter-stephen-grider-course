import React from "react";
import { Text, StyleSheet } from "react-native";

const ComponentsScreen = () => {
    const greeting = "Hello ComponentsScreen";
    return <Text style={styles.textStyle}>{greeting}</Text>;
};

const styles = StyleSheet.create({
    textStyle: {
        fontSize: 40
    }
});

export default ComponentsScreen;