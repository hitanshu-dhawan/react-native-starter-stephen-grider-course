import React, { useState } from "react";
import { StyleSheet, View, Text, TextInput, Button } from "react-native";

const TextScreen = () => {

    const [name, setName] = useState("")

    return (
        <View>
            <Text style={{ marginLeft: 16, marginTop: 16, marginRight: 16 }}>Name:</Text>
            <TextInput
                style={styles.input}
                value={name}
                onChangeText={text => setName(text)}
            />
            <Text style={{ margin: 16, fontSize: 20 }}>Hello {name}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    input: {
        margin: 16,
        padding: 8,
        borderColor: 'black',
        borderWidth: 1
    }
});

export default TextScreen;