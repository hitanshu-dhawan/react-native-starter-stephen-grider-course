import React from "react";
import { Text, StyleSheet, FlatList } from "react-native";

const ListScreen = () => {

    const friends = [
        { name: "Number 1" },
        { name: "Number 2" },
        { name: "Number 3" }
    ];

    return (
        <FlatList
            data={friends}
            renderItem={({ item }) => {
                return <Text>{item.name}</Text>
            }}
            keyExtractor={(item, index) => item.name}
        />
    );
};

export default ListScreen;