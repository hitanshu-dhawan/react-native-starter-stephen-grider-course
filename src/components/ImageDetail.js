import React from "react";
import { Text, StyleSheet, Image, View } from "react-native";

const ImageDetail = (props) => {
    return (
        <View>
            <Image source={props.imageSource} />
            <Text style={styles.textStyle}>Hey, {props.title}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    textStyle: {
        fontSize: 24
    }
});

export default ImageDetail;